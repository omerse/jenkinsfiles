pipeline {
    agent any
    parameters {
        choice(name: 'CHOICE', choices: ['One', 'Two', 'Three'], description: 'Pick something')
    }
    stages {
        stage("Build") {
            steps {
                echo "YESH!"
                echo "${params.CHOICE} World!"
            }
        }
        stage("Integration Tests") {
            steps {
                sleep 2
            }
        }
        stage("Functional Tests") {
            steps {
                sh '''touch file.txt'''
            }
        }
        stage("Deploy") {
            steps {
                sh 'echo Deploy'
            }
        }

    }
}